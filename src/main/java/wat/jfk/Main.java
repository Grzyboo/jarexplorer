package wat.jfk;

import javax.swing.*;

public class Main {
    private static JFrame frame;

    public static void main(String[] args) {
        JFrame frame = new MainFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        Main.frame = frame;
    }

    public static JFrame getFrame() {
        return frame;
    }
}
