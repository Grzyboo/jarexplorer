package wat.jfk.jar.classes.body;

import wat.jfk.jar.mvc.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class BodyView extends View<BodyModel> {
    private JPanel panel;

    private JButton addField;
    private JButton addConstructor;
    private JButton addMethod;
    private JButton modifyItem;
    private JButton deleteItem;

    public BodyView(BodyModel bodyModel) {
        super(bodyModel);
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5,5,5,5);
        JPanel addingButtonsPanel = new JPanel(new GridBagLayout());
        addField = new JButton("Add field");
        addConstructor = new JButton("Add constructor");
        addMethod = new JButton("Add method");
        gbc.gridy = 0;
        addingButtonsPanel.add(addField, gbc);
        gbc.gridy = 1;
        addingButtonsPanel.add(addConstructor, gbc);
        gbc.gridy = 2;
        addingButtonsPanel.add(addMethod, gbc);

        JPanel modifyingButtonsPanel = new JPanel(new GridBagLayout());
        modifyItem = new JButton("Modify");
        deleteItem = new JButton("Delete");
        gbc.gridy = 0;
        modifyingButtonsPanel.add(modifyItem, gbc);
        gbc.gridy = 1;
        modifyingButtonsPanel.add(deleteItem, gbc);

        panel.add(addingButtonsPanel);
        panel.add(modifyingButtonsPanel);

        disableAllButtons();
    }

    void setModifyButtonEnabled(boolean enabled) {
        modifyItem.setEnabled(enabled);
    }

    void setDeleteButtonEnabled(boolean enabled) {
        deleteItem.setEnabled(enabled);
    }

    void setAddingButtonsEnabled(boolean enabled) {
        setAddingButtonsEnabled(enabled, enabled, enabled);
    }

    void setAddingButtonsEnabled(boolean fieldsEnabled, boolean constructorsEnabled, boolean methodsEnabled) {
        addField.setEnabled(fieldsEnabled);
        addConstructor.setEnabled(constructorsEnabled);
        addMethod.setEnabled(methodsEnabled);
    }

    void setButtonsListeners(ActionListener field, ActionListener constructor, ActionListener method, ActionListener modify, ActionListener delete) {
        addField.addActionListener(field);
        addConstructor.addActionListener(constructor);
        addMethod.addActionListener(method);
        modifyItem.addActionListener(modify);
        deleteItem.addActionListener(delete);
    }

    @Override
    public void refresh() {

    }

    @Override
    public Component getContent() {
        return panel;
    }

    public void disableAllButtons() {
        setAddingButtonsEnabled(false);
        setModifyButtonEnabled(false);
        setDeleteButtonEnabled(false);
    }
}
