package wat.jfk.jar.classes.body;

public interface FieldCreator {
    boolean createField(String str);
}
