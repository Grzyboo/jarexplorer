package wat.jfk.jar.classes.body;

public interface MethodCreator {
    boolean createMethod(String src);
}
