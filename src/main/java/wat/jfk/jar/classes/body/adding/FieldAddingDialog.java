package wat.jfk.jar.classes.body.adding;

import wat.jfk.jar.classes.body.FieldCreator;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class FieldAddingDialog extends MemberCreatingDialog {
    private JTextField fieldSource = new JTextField();

    private JTextField fullName = new JTextField();
    private JTextField error = new JTextField();

    private ModifiersPanel modifiersPanel;
    private FieldCreator fieldCreator;

    public FieldAddingDialog(FieldCreator fieldCreator) {
        this.fieldCreator = fieldCreator;

        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        modifiersPanel = new ModifiersPanel(e -> setFullName());
        add(modifiersPanel);

        add(fieldSource);
        add(fullName);
        add(error);
        fullName.setEditable(false);

        error.setDisabledTextColor(Color.RED);
        error.setEditable(false);

        DocumentListener onTextFieldChange = new DocumentListener() {
            @Override public void insertUpdate(DocumentEvent e) {
                setFullName();
            }
            @Override public void removeUpdate(DocumentEvent e) {
                setFullName();
            }
            @Override public void changedUpdate(DocumentEvent e) {
                setFullName();
            }
        };

        fieldSource.getDocument().addDocumentListener(onTextFieldChange);

        addDialogButtons();
    }

    private void setFullName() {
        String modifiers = Modifier.toString(modifiersPanel.getAccessModifier());
        String fieldName = fieldSource.getText();
        fullName.setText(modifiers + " " + fieldName);
    }

    private class ErrorChecker {
        private boolean check;
        private String text;

        ErrorChecker(boolean check, String text) {
            this.check = check;
            this.text = text;
        }

        boolean hasError() {
            if(check) {
                System.out.println("check is positive: " + text);
                error.setText(text);
            }

            return check;
        }
    }

    @Override
    void onAddClicked() {
        String src = fieldSource.getText();
        Pattern fieldNamePattern = Pattern.compile("([a-zA-Z_][a-zA-Z0-9_.]*[ \t]*)*;*");

        java.util.List<ErrorChecker> errorCheckers = new ArrayList<>();
        errorCheckers.add(new ErrorChecker(src.length() < 3, "The field source code needs to be longer"));
        errorCheckers.add(new ErrorChecker(!fieldNamePattern.matcher(src).matches(), "The field source code is incorrect"));

        for(ErrorChecker checker : errorCheckers) {
            if(checker.hasError())
                return;
        }

        boolean created = fieldCreator.createField(javassist.Modifier.toString(modifiersPanel.getAccessModifier()) + " " + src + ";");
        if(created)
            dispose();
    }

}
