package wat.jfk.jar.classes.body.adding;

import javassist.Modifier;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

class ModifiersPanel extends JPanel {
    private JRadioButton isPrivate = new JRadioButton("private");
    private JRadioButton isPackagePrivate = new JRadioButton("package-private");
    private JRadioButton isProtected = new JRadioButton("protected");
    private JRadioButton isPublic = new JRadioButton("public");

    ModifiersPanel(ActionListener onAnyButtonChanged) {
        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new BoxLayout(radioPanel, BoxLayout.X_AXIS));
        ButtonGroup group = new ButtonGroup();

        for (JRadioButton button : new JRadioButton[]{isPrivate, isPackagePrivate, isProtected, isPublic}) {
            group.add(button);
            radioPanel.add(button);
            button.addActionListener(onAnyButtonChanged);
        }

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(radioPanel);

        isPrivate.setSelected(true);
    }

    int getAccessModifier() {
        int modifier = 0;

        if(isPrivate.isSelected())
            return Modifier.setPrivate(modifier);
        else if(isPackagePrivate.isSelected())
             return Modifier.setPackage(modifier);
        else if(isProtected.isSelected())
            return Modifier.setProtected(modifier);
        else
            return Modifier.setPublic(modifier);
    }
}
