package wat.jfk.jar.classes.body.adding;

import wat.jfk.jar.classes.body.ConstructorCreator;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Modifier;

public class ConstructorAddingDialog extends MemberCreatingDialog {

    private ModifiersPanel modifiersPanel;
    private JTextArea constructorBody = new JTextArea();
    private ConstructorCreator constructorCreator;

    public ConstructorAddingDialog(ConstructorCreator constructorCreator) {
        this.constructorCreator = constructorCreator;

        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        modifiersPanel = new ModifiersPanel(e -> {});
        add(modifiersPanel);

        add(constructorBody);
        constructorBody.setPreferredSize(new Dimension(300, 300));

        addDialogButtons();
    }

    @Override
    void onAddClicked() {
        String modifiers = Modifier.toString(modifiersPanel.getAccessModifier());
        String body = constructorBody.getText();

        boolean created = constructorCreator.createConstructor(modifiers + " " + body);
        if(created)
            dispose();
    }
}
