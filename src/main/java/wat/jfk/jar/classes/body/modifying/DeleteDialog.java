package wat.jfk.jar.classes.body.modifying;

import javassist.*;
import wat.jfk.jar.classes.body.MemberProvider;
import wat.jfk.jar.mvc.Refreshable;
import wat.jfk.jar.mvc.View;

import javax.swing.*;

public class DeleteDialog extends MemberChangingDialog {
    private Refreshable viewToRefresh;

    public DeleteDialog(MemberProvider memberProvider, Refreshable viewToRefresh) {
        super(memberProvider);
        this.viewToRefresh = viewToRefresh;

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        CtMember member = memberProvider.getSelectedMember();
        String question = "Do you really want to delete " + getMemberType(member) + " named '" + member.getName() + "'?";

        JTextField questionField = new JTextField(question);
        questionField.setEditable(false);
        add(questionField);

        addFooterButtons("Delete");
    }

    private String getMemberType(CtMember member) {
        if(member instanceof CtField)
            return "field";
        else if(member instanceof CtConstructor)
            return "constructor";
        else if(member instanceof CtMethod)
            return "method";

        return "attribute";
    }

    @Override
    void onAcceptClicked() {
        CtMember member = memberProvider.getSelectedMember();

        if(member == null)
            return;

        try {
            CtClass clazz = memberProvider.getSelectedJarItem().getClassObject();
            if(member instanceof CtField)
                clazz.removeField((CtField)member);
            else if(member instanceof CtConstructor)
                clazz.removeConstructor((CtConstructor)member);
            else if(member instanceof CtMethod)
                clazz.removeMethod((CtMethod)member);

            dispose();

            if(viewToRefresh != null)
                viewToRefresh.refresh();

        } catch (NotFoundException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error when deleting a field", JOptionPane.ERROR_MESSAGE);
        }
    }
}
