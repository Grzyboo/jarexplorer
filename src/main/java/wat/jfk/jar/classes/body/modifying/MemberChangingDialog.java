package wat.jfk.jar.classes.body.modifying;

import wat.jfk.jar.classes.body.MemberProvider;

import javax.swing.*;
import java.awt.event.ActionListener;

abstract class MemberChangingDialog extends JDialog {
    MemberProvider memberProvider;

    private JButton accept = new JButton("Accept");
    private JButton cancel = new JButton("Cancel");

    MemberChangingDialog(MemberProvider memberProvider) {
        this.memberProvider = memberProvider;
    }

    void addFooterButtons(String acceptName) {
        accept.setText(acceptName);

        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.X_AXIS));
        optionsPanel.add(accept);
        optionsPanel.add(cancel);

        accept.addActionListener(e -> onAcceptClicked());
        cancel.addActionListener(e -> dispose());

        add(optionsPanel);
    }

    abstract void onAcceptClicked();
}
