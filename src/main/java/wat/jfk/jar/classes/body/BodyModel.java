package wat.jfk.jar.classes.body;

import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMember;
import javassist.CtMethod;
import wat.jfk.jar.content.JarItem;

class BodyModel implements MemberProvider {
    private JarItem selectedJarItem;
    private CtMember selectedMember;

    @Override
    public JarItem getSelectedJarItem() {
        return selectedJarItem;
    }

    public void setSelectedJarItem(JarItem selectedJarItem) {
        this.selectedJarItem = selectedJarItem;
    }

    boolean isField() {
        return selectedMember instanceof CtField;
    }

    boolean isConstructor() {
        return selectedMember instanceof CtConstructor;
    }

    boolean isMethod() {
        return selectedMember instanceof CtMethod;
    }

    void setSelectedMember(CtMember selected) {
        this.selectedMember = selected;
    }

    @Override
    public CtMember getSelectedMember() {
        return selectedMember;
    }

}
