package wat.jfk.jar.classes.body;

import javassist.*;
import wat.jfk.jar.classes.ClassController;
import wat.jfk.jar.classes.body.adding.ConstructorAddingDialog;
import wat.jfk.jar.classes.body.adding.FieldAddingDialog;
import wat.jfk.jar.classes.body.adding.MethodAddingDialog;
import wat.jfk.jar.classes.body.modifying.DeleteDialog;
import wat.jfk.jar.classes.body.modifying.ModifyDialog;
import wat.jfk.jar.content.ItemType;
import wat.jfk.jar.content.JarItem;
import wat.jfk.jar.mvc.Controller;
import wat.jfk.jar.mvc.Refreshable;

import javax.swing.*;

public class BodyController extends Controller<BodyView, BodyModel> implements FieldCreator, ConstructorCreator, MethodCreator {
    private ClassController classController;
    private Refreshable viewToRefresh;

    public BodyController() {
        super();

        view.setButtonsListeners(e -> showDialog(new FieldAddingDialog(this)),
                e -> showDialog(new ConstructorAddingDialog(this)),
                e -> showDialog(new MethodAddingDialog(this)),
                e -> showDialog(new ModifyDialog(model)),
                e -> showDialog(new DeleteDialog(model, viewToRefresh))
        );
    }

    public void setDeleteDialogRefreshable(Refreshable refreshable) {
        this.viewToRefresh = refreshable;
    }

    public void setClassController(ClassController classController) {
        this.classController = classController;
    }

    public void currentlySelectedItemDeleted() {
        view.disableAllButtons();
    }

    private void showDialog(JDialog dialog) {
        dialog.pack();
        dialog.setModal(true);
        dialog.setVisible(true);
    }

    public void setSelectedMember(CtMember selected) {
        model.setSelectedMember(selected);
        view.setModifyButtonEnabled(false);
        view.setDeleteButtonEnabled(false);

        if(model.isField()) {
            view.setModifyButtonEnabled(false);
            view.setDeleteButtonEnabled(true);
        }
        else if(model.isConstructor()) {
            view.setModifyButtonEnabled(true);
            view.setDeleteButtonEnabled(true);
        }
        else if(model.isMethod()) {
            view.setModifyButtonEnabled(true);
            view.setDeleteButtonEnabled(true);
        }
    }

    public void jarItemSelected(JarItem item) {
        model.setSelectedJarItem(item);

        if(item.getType() == ItemType.CLASS) {
            view.setAddingButtonsEnabled(true);
        }
        else if(item.getType() == ItemType.INTERFACE) {
            view.setAddingButtonsEnabled(false, false, true);
        }
        else {
            view.setAddingButtonsEnabled(false);
        }

        view.setModifyButtonEnabled(false);
        view.setDeleteButtonEnabled(false);
    }

    @Override
    public boolean createField(String src) {
        try {
            CtClass clazz = model.getSelectedJarItem().getClassObject();
            CtField field = CtField.make(src, clazz);
            clazz.addField(field);
        } catch (NotFoundException | CannotCompileException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(view.getContent(), e.getMessage(), "Error when creating a field", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        classController.refresh();
        return true;
    }

    @Override
    public boolean createConstructor(String src) {
        try {
            CtClass clazz = model.getSelectedJarItem().getClassObject();
            CtConstructor constructor = CtNewConstructor.make(src, clazz);
            clazz.addConstructor(constructor);
        } catch (NotFoundException | CannotCompileException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(view.getContent(), e.getMessage(), "Error when creating a constructor", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        classController.refresh();
        return true;
    }

    @Override
    public boolean createMethod(String src) {
        try {
            CtClass clazz = model.getSelectedJarItem().getClassObject();
            CtMethod method = CtMethod.make(src, clazz);
            clazz.addMethod(method);
        } catch (NotFoundException | CannotCompileException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(view.getContent(), e.getMessage(), "Error when creating a method", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        classController.refresh();
        return true;
    }
}
