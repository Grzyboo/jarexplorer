package wat.jfk.jar.classes.body.adding;

import wat.jfk.jar.classes.body.MethodCreator;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Modifier;

public class MethodAddingDialog extends MemberCreatingDialog {
    private JTextArea methodBody = new JTextArea();
    private ModifiersPanel modifiersPanel;
    private MethodCreator methodCreator;

    public MethodAddingDialog(MethodCreator methodCreator) {
        this.methodCreator = methodCreator;

        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        modifiersPanel = new ModifiersPanel(e -> {});
        add(modifiersPanel);
        add(methodBody);
        methodBody.setPreferredSize(new Dimension(300, 300));

        addDialogButtons();
    }

    @Override
    void onAddClicked() {
        String modifiers = Modifier.toString(modifiersPanel.getAccessModifier());
        String body = methodBody.getText();

        boolean created = methodCreator.createMethod(modifiers + " " + body);
        if(created)
            dispose();
    }
}
