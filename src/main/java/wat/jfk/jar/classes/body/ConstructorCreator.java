package wat.jfk.jar.classes.body;

public interface ConstructorCreator {
    boolean createConstructor(String src);
}
