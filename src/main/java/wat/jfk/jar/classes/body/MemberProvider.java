package wat.jfk.jar.classes.body;

import javassist.CtMember;
import wat.jfk.jar.content.JarItem;

public interface MemberProvider {
    CtMember getSelectedMember();
    JarItem getSelectedJarItem();
}
