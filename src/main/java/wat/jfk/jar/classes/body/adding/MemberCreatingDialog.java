package wat.jfk.jar.classes.body.adding;

import javax.swing.*;

abstract class MemberCreatingDialog extends JDialog {
    private JButton add = new JButton("Add");
    private JButton cancel = new JButton("Cancel");

    void addDialogButtons() {
        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.X_AXIS));
        optionsPanel.add(add);
        optionsPanel.add(cancel);

        add.addActionListener(e -> onAddClicked());
        cancel.addActionListener(e -> dispose());

        add(optionsPanel);
    }

    abstract void onAddClicked();
}
