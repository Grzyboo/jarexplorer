package wat.jfk.jar.classes.body.modifying;

import javassist.CannotCompileException;
import javassist.CtBehavior;
import wat.jfk.jar.classes.body.MemberProvider;

import javax.swing.*;
import java.awt.*;

public class ModifyDialog extends MemberChangingDialog {

    private JRadioButton insertBefore = new JRadioButton("Insert before");
    private JRadioButton replaceBody = new JRadioButton("Replace body");
    private JRadioButton insertAfter = new JRadioButton("Insert after");

    private JTextArea body = new JTextArea();

    public ModifyDialog(MemberProvider memberProvider) {
        super(memberProvider);

        ButtonGroup group = new ButtonGroup();
        group.add(insertBefore);
        group.add(replaceBody);
        group.add(insertAfter);
        replaceBody.setSelected(true);

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(insertBefore);
        add(replaceBody);
        add(insertAfter);

        body.setPreferredSize(new Dimension(300, 300));
        add(body);

        addFooterButtons("Save");
    }

    @Override
    void onAcceptClicked() {
        String src = body.getText();
        CtBehavior behavior = (CtBehavior) memberProvider.getSelectedMember();

        try {
            if(insertBefore.isSelected())
                behavior.insertBefore(src);
            else if(replaceBody.isSelected())
                behavior.setBody(src);
            else if(insertAfter.isSelected())
                behavior.insertAfter(src);

            dispose();

        } catch (CannotCompileException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error when modifying body", JOptionPane.ERROR_MESSAGE);
        }
    }

}
