package wat.jfk.jar.classes;

enum ListType {
    FIELDS,
    CONSTRUCTORS,
    METHODS
}