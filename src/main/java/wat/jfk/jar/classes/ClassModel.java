package wat.jfk.jar.classes;

import javassist.*;
import wat.jfk.jar.content.ItemType;
import wat.jfk.jar.content.JarItem;

import java.io.IOException;

class ClassModel {
    private JarItem item;

    void setItem(JarItem item) {
        this.item = item;
    }

    DisplayMethod[] getMethods() {
        try {
            return getMethods(item);
        } catch (NotFoundException e) { }

        return new DisplayMethod[] {};
    }

    private DisplayMethod[] getMethods(JarItem item) throws NotFoundException {
        ClassPool classPool = ClassPool.getDefault();
        CtClass clazz = classPool.get(item.getClassLocation());
        CtMethod[] methods = clazz.getDeclaredMethods();

        DisplayMethod[] displayMethods = new DisplayMethod[methods.length];
        for(int i = 0; i < methods.length; ++i)
            displayMethods[i] = new DisplayMethod(methods[i]);

        return displayMethods;
    }

    DisplayField[] getFields() {
        try {
            return getFields(item);
        } catch (NotFoundException e) { }

        return new DisplayField[] {};
    }

    private DisplayField[] getFields(JarItem item) throws NotFoundException {
        ClassPool classPool = ClassPool.getDefault();
        CtClass clazz = classPool.get(item.getClassLocation());
        CtField[] fields = clazz.getDeclaredFields();

        DisplayField[] displayFields = new DisplayField[fields.length];
        for(int i = 0; i < fields.length; ++i)
            displayFields[i] = new DisplayField(fields[i]);

        return displayFields;
    }

    DisplayConstructor[] getConstructors() {
        try {
            return getConstructors(item);
        } catch (NotFoundException e) { }

        return new DisplayConstructor[] {};
    }

    private DisplayConstructor[] getConstructors(JarItem item) throws NotFoundException {
        ClassPool classPool = ClassPool.getDefault();
        CtClass clazz = classPool.get(item.getClassLocation());
        CtConstructor[] constructors = clazz.getDeclaredConstructors();

        DisplayConstructor[] displayConstructors = new DisplayConstructor[constructors.length];
        for(int i = 0; i < constructors.length; ++i) {
            displayConstructors[i] = new DisplayConstructor(constructors[i]);
        }

        return displayConstructors;
    }

    ItemType getItemType() {
        return item == null ? null : item.getType();
    }

    String getJarFileName() {
        return item == null ? null : item.getJarFile();
    }

    String getJarItemName() {
        return item == null ? null : item.getName();
    }


    String getItemContent() {
        JarResourceFileExtractor extractor = new JarResourceFileExtractor(item.getJarFile());
        try {
            return new String(extractor.getFileContent(item.getOriginalLocation()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

}
