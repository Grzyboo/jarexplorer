package wat.jfk.jar.classes;

import javassist.*;

class DisplayConstructor implements DisplayMember {
    private CtConstructor constructor;

    private String simpleName;
    private String modifiers;
    private String parametersTypes;

    DisplayConstructor(CtConstructor constructor) {
        this.constructor = constructor;

        simpleName = constructor.getName();
        modifiers = Modifier.toString(constructor.getModifiers());
        try {
            StringBuilder params = new StringBuilder("(");
            CtClass[] types = constructor.getParameterTypes();
            for(int i = 0; i < types.length; ++i) {
                CtClass parameterType = types[i];
                params.append(parameterType.getSimpleName());

                if(i != types.length - 1)
                    params.append(", ");
            }
            params.append(")");

            parametersTypes = params.toString();
        } catch (NotFoundException e) {
            parametersTypes = "?";
        }
    }

    @Override
    public String toString() {
        return modifiers + " " + simpleName + parametersTypes;
    }

    @Override
    public CtMember getMember() {
        return constructor;
    }
}