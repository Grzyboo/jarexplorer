package wat.jfk.jar.classes;

import wat.jfk.jar.content.ItemType;
import wat.jfk.jar.mvc.View;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassView extends View<ClassModel> {
    private JPanel panel;
    private JPanel membersPanel;
    private JLabel fieldsLabel;
    private JList<DisplayField> fields;
    private JLabel constructorsLabel;
    private JList<DisplayConstructor> constructors;
    private JLabel methodsLabel;
    private JList<DisplayMethod> methods;
    private JLabel typeField;
    private JTextArea textArea;
    private javax.swing.ImageIcon imageIcon;
    private JLabel imageLabel;

    public ClassView(ClassModel classModel) {
        super(classModel);

        panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        typeField = new JLabel();

        textArea = new JTextArea();
        textArea.setVisible(false);
        textArea.setMaximumSize(new Dimension(600, 400));

        fields = new JList<>();
        fields.setBackground(new Color(55, 255, 55));
        constructors = new JList<>();
        constructors.setBackground(new Color(255, 255, 55));
        methods = new JList<>();
        methods.setBackground(new Color(55, 255, 255));

        imageIcon = new ImageIcon();
        imageLabel = new JLabel(imageIcon);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 10, 10, 10);

        gbc.anchor = GridBagConstraints.NORTH;
        membersPanel = createMembersPanel();
        gbc.gridy = 0;
        panel.add(typeField, gbc);
        gbc.gridy = 1;
        panel.add(membersPanel, gbc);
        gbc.gridy = 2;
        panel.add(textArea, gbc);
        gbc.gridy = 3;
        panel.add(imageLabel, gbc);
        gbc.gridy = 4;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.VERTICAL;
        panel.add(new JPanel(), gbc);   // Spacer
    }

    private JPanel createMembersPanel() {
        GridBagConstraints gbc = new GridBagConstraints();
        JPanel membersPanel = new JPanel(new GridBagLayout());
        gbc.insets = new Insets(5,15,5,15);
        gbc.anchor = GridBagConstraints.NORTH;

        gbc.gridx = 0;
        gbc.gridy = 0;
        fieldsLabel = new JLabel("Fields");
        membersPanel.add(fieldsLabel, gbc);
        gbc.gridx = 1;
        constructorsLabel = new JLabel("Constructors");
        membersPanel.add(constructorsLabel, gbc);
        gbc.gridx = 2;
        methodsLabel = new JLabel("Methods");
        membersPanel.add(methodsLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        membersPanel.add(fields, gbc);
        gbc.gridx = 1;
        membersPanel.add(constructors, gbc);
        gbc.gridx = 2;
        membersPanel.add(methods, gbc);

        membersPanel.setVisible(false);

        return membersPanel;
    }

    void setSelectionListeners(ListSelectionListener fieldsListener, ListSelectionListener constructorsListener, ListSelectionListener methodsListener) {
        fields.addListSelectionListener(fieldsListener);
        constructors.addListSelectionListener(constructorsListener);
        methods.addListSelectionListener(methodsListener);
    }


    void clearSelectionsExcept(ListType selected) {
        if(selected == ListType.FIELDS) {
            constructors.clearSelection();
            methods.clearSelection();
        }
        if(selected == ListType.CONSTRUCTORS) {
            fields.clearSelection();
            methods.clearSelection();
        }
        if(selected == ListType.METHODS) {
            fields.clearSelection();
            constructors.clearSelection();
        }
    }

    void hideItems() {
        typeField.setText("");
        textArea.setVisible(false);
        imageLabel.setVisible(false);
        membersPanel.setVisible(false);
    }

    @Override
    public void refresh() {
        ItemType type = model.getItemType();

        if(type == null)
            return;

        fields.setListData(model.getFields());
        constructors.setListData(model.getConstructors());
        methods.setListData(model.getMethods());

        if(type == ItemType.CLASS || type == ItemType.INTERFACE) {
            membersPanel.setVisible(true);
            if(type == ItemType.CLASS) {
                fieldsLabel.setVisible(true);
                constructorsLabel.setVisible(true);
                fields.setVisible(true);
                constructors.setVisible(true);
            }
            else {
                fieldsLabel.setVisible(false);
                constructorsLabel.setVisible(false);
                fields.setVisible(false);
                constructors.setVisible(false);
            }
        }
        else {
            if(type == ItemType.TEXT || type == ItemType.MANIFEST) {
                textArea.setText(model.getItemContent());
                textArea.setVisible(true);
            }
            else if(type == ItemType.IMAGE) {
                try(URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new URL("file:/" + model.getJarFileName())})) {
                    URL url = urlClassLoader.getResource(model.getJarItemName());

                    if(url != null) {
                        BufferedImage image = ImageIO.read(url);
                        imageIcon.setImage(image);
                        imageLabel.setVisible(true);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        typeField.setText("Currently selected item: " + model.getItemType().toString());
    }

    @Override
    public Component getContent() {
        return panel;
    }
}
