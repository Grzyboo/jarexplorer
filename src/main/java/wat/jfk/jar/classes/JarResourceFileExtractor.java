package wat.jfk.jar.classes;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class JarResourceFileExtractor {
    private String fileName;

    public JarResourceFileExtractor(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileContent(String resourceJarLocation) throws IOException {
        try(JarInputStream jis = new JarInputStream(new FileInputStream(fileName))) {
            JarEntry je;
            while ((je = jis.getNextJarEntry()) != null) {
                if (je.getName().equals(resourceJarLocation)) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    while (true) {
                        int qwe = jis.read();
                        if (qwe == -1)
                            break;
                        baos.write(qwe);
                    }
                    return baos.toByteArray();
                }
            }
        }
        return new byte[0];
    }
}
