package wat.jfk.jar.classes;

import javassist.*;

class DisplayMethod implements DisplayMember {
    private CtMethod method;

    private String simpleName;
    private String modifiers;
    private String type;
    private String parametersTypes;

    DisplayMethod(CtMethod method) {
        this.method = method;

        try {
            type = method.getReturnType().getSimpleName();
        } catch (NotFoundException e) {
            type = "?";
        }

        simpleName = method.getName();
        modifiers = Modifier.toString(method.getModifiers());
        try {
            StringBuilder params = new StringBuilder("(");
            CtClass[] types = method.getParameterTypes();
            for(int i = 0; i < types.length; ++i) {
                CtClass parameterType = types[i];
                params.append(parameterType.getSimpleName());

                if(i != types.length - 1)
                    params.append(", ");
            }
            params.append(")");

            parametersTypes = params.toString();
        } catch (NotFoundException e) {
            parametersTypes = "?";
        }
    }

    @Override
    public String toString() {
        return modifiers + (modifiers.length() > 0 ? " " : "") + type + " "  + simpleName + parametersTypes;
    }

    @Override
    public CtMember getMember() {
        return method;
    }
}
