package wat.jfk.jar.classes;

import javassist.CtMember;
import wat.jfk.jar.classes.body.BodyController;
import wat.jfk.jar.content.JarItem;
import wat.jfk.jar.mvc.Controller;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ClassController extends Controller<ClassView, ClassModel> {
    private BodyController bodyController;

    public ClassController(BodyController bodyController) {
        super();
        this.bodyController = bodyController;
        bodyController.setDeleteDialogRefreshable(view);
        view.setSelectionListeners(new Selector(ListType.FIELDS), new Selector(ListType.CONSTRUCTORS), new Selector(ListType.METHODS));
    }

    public void setSelectedClass(JarItem selected) {
        model.setItem(selected);
        view.refresh();
    }

    public void currentlySelectedItemDeleted() {
        model.setItem(null);
        view.hideItems();
    }

    public void refresh() {
        view.refresh();
    }

    private class Selector implements ListSelectionListener {
        private ListType listType;

        Selector(ListType listType) {
            this.listType = listType;
        }

        @Override
        @SuppressWarnings("unchecked cast")
        public void valueChanged(ListSelectionEvent e) {
            if(e.getValueIsAdjusting()) {
                view.clearSelectionsExcept(listType);

                JList<? extends DisplayMember> list = (JList<? extends DisplayMember>)e.getSource();
                CtMember member = list.getSelectedValue().getMember();
                bodyController.setSelectedMember(member);
            }
        }
    }
}
