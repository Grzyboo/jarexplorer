package wat.jfk.jar.classes;

import javassist.CtField;
import javassist.CtMember;
import javassist.Modifier;
import javassist.NotFoundException;

class DisplayField implements DisplayMember {
    private CtField field;
    private String type;
    private String modifiers;
    private String simpleName;

    DisplayField(CtField field) {
        this.field = field;

        try {
            type = field.getType().getSimpleName();
        } catch (NotFoundException e) {
            type = "?";
        }

        modifiers = Modifier.toString(field.getModifiers());
        simpleName = field.getName();
    }

    @Override
    public String toString() {
        return modifiers + (modifiers.length() > 0 ? " " : "") + type + " " + simpleName;
    }

    @Override
    public CtMember getMember() {
        return field;
    }
}