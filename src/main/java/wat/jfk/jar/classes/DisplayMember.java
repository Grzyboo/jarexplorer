package wat.jfk.jar.classes;

import javassist.CtMember;

public interface DisplayMember {
    CtMember getMember();
}
