package wat.jfk.jar.content;

import javassist.*;
import wat.jfk.jar.classes.ClassController;
import wat.jfk.jar.classes.body.BodyController;
import wat.jfk.jar.mvc.Controller;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class JarContentController extends Controller<JarContentView, JarContentModel> {
    private BodyController bodyController;
    private ClassController classController;

    private JarFileCreator jarFileCreator;

    public JarContentController(BodyController bodyController, ClassController classController) {
        super();
        this.bodyController = bodyController;
        this.classController = classController;

        view.onTreeItemSelected(this::onTreeItemSelected);

        view.addPackageButtonListener(new PackageOnClickListener());
        view.addClassButtonListener(new ClassOnClickListener());
        view.addInterfaceButtonListener(new InterfaceOnClickListener());
        view.addDeleteButtonListener(new DeleteOnClickListener());

        jarFileCreator = new JarFileCreator();
    }

    private void onTreeItemSelected(DefaultMutableTreeNode selected) {
        JarItem jarItem = (JarItem) selected.getUserObject();
        classController.setSelectedClass(jarItem);
        bodyController.jarItemSelected(jarItem);
        model.setCurrentlySelectedNode(selected);
    }

    public void setListenersForMenuItems(JMenuItem load, JMenuItem save) {
        load.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Jar files", "jar");
            chooser.setFileFilter(filter);
            chooser.setCurrentDirectory(new File("."));

            int returnVal = chooser.showOpenDialog(view.getContent());
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();
                model.setJarFileName(selectedFile.getAbsolutePath());

                try {
                    ClassPool.getDefault().insertClassPath(selectedFile.getAbsolutePath());
                } catch (NotFoundException ex) {
                    ex.printStackTrace();
                }

                try {
                    model.createJarTree(selectedFile.getAbsolutePath());
                    view.refresh();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        save.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Jar files", "jar");
            chooser.setFileFilter(filter);
            chooser.setCurrentDirectory(new File("."));

            int returnVal = chooser.showSaveDialog(view.getContent());
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();

                try {
                    jarFileCreator.saveJarToFile(model.getJarFileName(), selectedFile, (DefaultMutableTreeNode)model.getTreeRoot());
                    JOptionPane.showMessageDialog(null, "Successfully saved jar file: " + selectedFile.getName(), "Jar Saved", JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Error when saving a jar file: " + ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace();
                }
            }
        });
    }

    private class PackageOnClickListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String packageName = JOptionPane.showInputDialog(view.getContent(), "Enter package name", "New package", JOptionPane.PLAIN_MESSAGE);
            if(packageName != null)
                view.insertNode(model.createNewPackageOnSelectedItem(packageName), model.getCurrentlySelectedNode());
        }
    }

    private class ClassOnClickListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String className = JOptionPane.showInputDialog(view.getContent(), "Enter class name", "New class", JOptionPane.PLAIN_MESSAGE);
            if(className != null)
                view.insertNode(model.createNewClassOnSelectedItem(className), model.getCurrentlySelectedNode());
        }
    }

    private class InterfaceOnClickListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String className = JOptionPane.showInputDialog(view.getContent(), "Enter interface name", "New interface", JOptionPane.PLAIN_MESSAGE);
            if(className != null)
                view.insertNode(model.createNewInterfaceOnSelectedItem(className), model.getCurrentlySelectedNode());
        }
    }

    private class DeleteOnClickListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String type;

            JarItem item = model.getCurrentlySelectedItem();
            if((item.getType() == ItemType.PACKAGE || item.getType() == ItemType.CLASS || item.getType() == ItemType.INTERFACE) && item.isAddedThisSession())
                type = item.getType().name().toLowerCase();
            else {
                JOptionPane.showMessageDialog(view.getContent(), "Invalid attempt to delete item that should not be deleted");
                return;
            }

            int result = JOptionPane.showConfirmDialog(view.getContent(), "Do you want to delete " + type + " '" + item.getName() + "'?", "Item deletion", JOptionPane.OK_CANCEL_OPTION);
            if(result == JOptionPane.OK_OPTION) {
                view.deleteNode(model.getCurrentlySelectedNode());
                view.setButtonsEnabled(false);
                model.setCurrentlySelectedNode(null);

                classController.currentlySelectedItemDeleted();
                bodyController.currentlySelectedItemDeleted();
            }
        }
    }

}
