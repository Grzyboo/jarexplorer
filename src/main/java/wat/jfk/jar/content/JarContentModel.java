package wat.jfk.jar.content;

import javassist.ClassPool;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

class JarContentModel {
    private String jarFileName;
    private JarFilesTree jarFilesTree;
    private DefaultMutableTreeNode currentlySelectedNode;

    public void createJarTree(String fileName) throws IOException {
        JarInputStream jis = new JarInputStream(new FileInputStream(fileName));
        List<String> entries = new ArrayList<>();

        JarEntry je;
        while((je = jis.getNextJarEntry()) != null) {
            entries.add(je.getName());
        }

        if(jis.getManifest() != null) {
            if(!entries.contains("META-INF/"))
                entries.add("META-INF/");
            if(!entries.contains("META-INF/MANIFEST.MF"))
                entries.add("META-INF/MANIFEST.MF");
        }

        entries.sort(Comparator.comparingInt(String::length));

        jarFilesTree = new JarFilesTree(fileName, entries);
        jis.close();
    }

    public TreeNode getTreeRoot() {
        return jarFilesTree == null ? new DefaultMutableTreeNode() : jarFilesTree.getRoot();
    }

    void setCurrentlySelectedNode(DefaultMutableTreeNode node) {
        currentlySelectedNode = node;
    }

    DefaultMutableTreeNode getCurrentlySelectedNode() {
        return currentlySelectedNode;
    }

    JarItem getCurrentlySelectedItem() {
        return (JarItem) currentlySelectedNode.getUserObject();
    }

    private boolean isNameAlreadyInCurrentlySelectedNode(String name) {
        for(int i = 0; i < currentlySelectedNode.getChildCount(); ++i) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) currentlySelectedNode.getChildAt(i);
            String childName = ((JarItem)child.getUserObject()).getName();
            if(name.equals(childName)) {
                return true;
            }
        }

        return false;
    }

    private DefaultMutableTreeNode createChildForCurrentlySelectedNode(String name, ItemType type) {
        if(type == ItemType.CLASS || type == ItemType.INTERFACE)
            name += ".class";

        if(isNameAlreadyInCurrentlySelectedNode(name))
            return null;

        JarItem parentItem = (JarItem)currentlySelectedNode.getUserObject();
        String fullName = parentItem.getClassLocation() + "/" + name;
        JarItem jarItem = new JarItem(name, fullName, type, parentItem.getJarFile());
        jarItem.setAddedThisSession(true);
        return new DefaultMutableTreeNode(jarItem, type == ItemType.PACKAGE);
    }

    DefaultMutableTreeNode createNewPackageOnSelectedItem(String name) {
        if(currentlySelectedNode != null) {
            DefaultMutableTreeNode createdNode = createChildForCurrentlySelectedNode(name, ItemType.PACKAGE);
            if(createdNode != null) {
                currentlySelectedNode.add(createdNode);
                return createdNode;
            }
        }

        return null;
    }

    DefaultMutableTreeNode createNewClassOnSelectedItem(String name) {
        if(currentlySelectedNode != null) {
            DefaultMutableTreeNode createdNode = createChildForCurrentlySelectedNode(name, ItemType.CLASS);

            if(createdNode != null) {
                currentlySelectedNode.add(createdNode);
                createClass((JarItem) createdNode.getUserObject());
                return createdNode;
            }
        }

        return null;
    }

    DefaultMutableTreeNode createNewInterfaceOnSelectedItem(String name) {
        if(currentlySelectedNode != null) {
            DefaultMutableTreeNode createdNode = createChildForCurrentlySelectedNode(name, ItemType.INTERFACE);

            if(createdNode != null) {
                currentlySelectedNode.add(createdNode);
                createInterface((JarItem) createdNode.getUserObject());
                return createdNode;
            }
        }

        return null;
    }

    private void createClass(JarItem jarItem) {
        ClassPool classPool = ClassPool.getDefault();
        classPool.makeClass(jarItem.getClassLocation());
    }

    private void createInterface(JarItem jarItem) {
        ClassPool classPool = ClassPool.getDefault();
        classPool.makeInterface(jarItem.getClassLocation());
    }

    public String getJarFileName() {
        return jarFileName;
    }

    public void setJarFileName(String jarFileName) {
        this.jarFileName = jarFileName;
    }

}
