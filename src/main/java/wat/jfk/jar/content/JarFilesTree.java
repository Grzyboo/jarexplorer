package wat.jfk.jar.content;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class JarFilesTree {
    private DefaultMutableTreeNode root;
    private String fileName;

    JarFilesTree(String fileName, List<String> entries) {
        root = new DefaultMutableTreeNode(new JarItem(fileName, "", ItemType.JAR_ROOT, fileName), true);
        this.fileName = fileName;

        for(String entry : entries)
            add(entry);
    }

    private void add(String item) {
        if(item.endsWith("/"))
            item = item.substring(0, item.length()-1);

        String[] split = item.split("/");
        putItemIntoCorrectBranch(root, split, 0, item);
    }

    private void putItemIntoCorrectBranch(DefaultMutableTreeNode root, String[] split, int currentIndex, String fullName) {
        if(currentIndex + 1 == split.length) {
            ItemType type = getItemType(split[currentIndex], fullName);
            root.add(new DefaultMutableTreeNode(new JarItem(split[currentIndex], fullName, type, fileName), type == ItemType.PACKAGE));
        }
        else {
            for (int i = 0; i < root.getChildCount(); ++i) {
                DefaultMutableTreeNode child = (DefaultMutableTreeNode) root.getChildAt(i);
                JarItem childItem = (JarItem) child.getUserObject();
                if (childItem.getName().equals(split[currentIndex])) {
                    putItemIntoCorrectBranch(child, split, currentIndex + 1, fullName);
                    break;
                }
            }
        }
    }

    private static final Pattern FILE_WITH_EXTENSION_PATTERN = Pattern.compile("(.*)\\.(.+)");

    private ItemType getItemType(String itemName, String fullName) {
        Matcher m = FILE_WITH_EXTENSION_PATTERN.matcher(itemName);

        if(itemName.equals("MANIFEST.MF"))
            return ItemType.MANIFEST;

        if(itemName.equals(".gitignore"))
            return ItemType.TEXT;

        if(m.matches()) {
            String extension = m.group(2);

            switch (extension.toLowerCase()) {
                case "class": {
                    try {
                        ClassPool classPool = ClassPool.getDefault();
                        CtClass clazz = classPool.get(fullName.replace(".class", "").replace("/", "."));
                        if(clazz.isInterface())
                            return ItemType.INTERFACE;
                    } catch (NotFoundException e) {
                        System.out.println("'" + fullName + "' not found");
                    }
                    return ItemType.CLASS;
                }
                case "txt":
                case "json":
                case "java":
                    return ItemType.TEXT;
                case "jpg":
                case "jpeg":
                case "png":
                case "gif":
                case "bmp":
                    return ItemType.IMAGE;
                case "mp3":
                case "wav":
                    return ItemType.RESOURCE;
            }
        }
        else {
            return ItemType.PACKAGE;
        }

        return ItemType.UNKNOWN;
    }

    TreeNode getRoot() {
        return root;
    }
}
