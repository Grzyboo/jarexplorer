package wat.jfk.jar.content;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

@SuppressWarnings("ResultOfMethodCallIgnored")
class JarFileCreator {
    private final String TEMP_DIR = "temp/";
    private final File TEMP_DIR_FILE = new File(TEMP_DIR);
    private final String COPY_FILENAME = "old.jar";

    private JarOutputStream jos;

    void saveJarToFile(String inputFileName, File outputFile, DefaultMutableTreeNode root) throws NotFoundException, CannotCompileException, IOException {
        //jos = new JarOutputStream(new FileOutputStream(outputFile));
        jos = new JarOutputStream(new FileOutputStream(new File("jos_Invaders.jar")));
        prepareEnvironment(inputFileName);

        parseChildren(root);

        String jarName = outputFile.getAbsolutePath();
        if(!jarName.endsWith(".jar"))
            jarName += ".jar";

        createJar(jarName);
        jos.close();
    }

    private void prepareEnvironment(String inputFileName) {
        try {
            deleteDirectory(TEMP_DIR_FILE);
            TEMP_DIR_FILE.mkdir();
            Files.copy(new File(inputFileName).toPath(), new File(TEMP_DIR, COPY_FILENAME).toPath());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createJar(String jarName) {
        File oldJarCopy = new File(TEMP_DIR, COPY_FILENAME);
        oldJarCopy.delete();

        Process p;
        try {
            p = Runtime.getRuntime().exec("jar --create --manifest META-INF/MANIFEST.MF --file " + jarName + " *", new String[]{""}, TEMP_DIR_FILE);
        } catch (IOException e) {
            return;
        }

        String line;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = in.readLine()) != null)
                System.out.println(line);
            in.close();

            in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while ((line = in.readLine()) != null)
                System.out.println(line);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        deleteDirectory(TEMP_DIR_FILE);
    }

    private void parseChildren(DefaultMutableTreeNode root) throws NotFoundException, CannotCompileException, IOException {
        for (int i = 0; i < root.getChildCount(); ++i) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) root.getChildAt(i);
            JarItem childItem = (JarItem) child.getUserObject();

            ItemType type = childItem.getType();
            if(type == ItemType.CLASS || type == ItemType.INTERFACE) {
                saveClass(childItem);
            }
            else if(type == ItemType.MANIFEST) {
                saveManifest(childItem);
            }
            else if(type == ItemType.PACKAGE) {
                savePackage(childItem);
            }
            else if(type != ItemType.PACKAGE) {
                saveResource(childItem);
            }

            parseChildren(child);
        }
    }

    private boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(files != null){
                for (File file : files) {
                    if (file.isDirectory())
                        deleteDirectory(file);
                    else
                        file.delete();
                }
            }
        }
        return(directory.delete());
    }

    private void saveClass(JarItem jarItem) throws NotFoundException, CannotCompileException, IOException {
        ClassPool cp = ClassPool.getDefault();
        String classLocation = jarItem.getClassLocation();
        CtClass clazz = cp.get(classLocation);
        clazz.defrost();
        clazz.writeFile(TEMP_DIR_FILE.getAbsolutePath());

        writeJarEntry(jarItem.getOriginalLocation(), clazz.toBytecode());
    }

    private void saveResource(JarItem jarItem) {
        try(URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new URL("file:/" + new File(TEMP_DIR, COPY_FILENAME).getAbsolutePath() )})) {
            URL url = urlClassLoader.getResource(jarItem.getOriginalLocation());

            if(url != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                URLConnection uc = url.openConnection();
                uc.setUseCaches(false); // Fix bug where you can't delete jar file that has been accessed at least once. is.close() doesn't work on its own

                try(InputStream is = uc.getInputStream()) {
                    byte[] byteChunk = new byte[4096];
                    int n;
                    while ( (n = is.read(byteChunk)) > 0 )
                        baos.write(byteChunk, 0, n);
                }
                catch (IOException e) {
                    System.err.printf ("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
                    e.printStackTrace ();
                }

                byte[] bytes = baos.toByteArray();
                baos.close();

                File file = new File(TEMP_DIR, jarItem.getOriginalLocation());
                file.getParentFile().mkdirs();
                file.createNewFile();

                try (FileOutputStream fos = new FileOutputStream(file)) {
                    fos.write(bytes);
                }

                writeJarEntry(jarItem.getOriginalLocation(), bytes);
            }
        } catch (IOException e) {
            System.out.println("ERROR in: " + jarItem.getOriginalLocation());
            e.printStackTrace();
        }
    }

    private void saveManifest(JarItem jarItem) {
        try {
            JarInputStream jis = new JarInputStream(new FileInputStream(new File(TEMP_DIR, COPY_FILENAME)));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            jis.getManifest().write(baos);

            File file = new File(TEMP_DIR, jarItem.getOriginalLocation());
            file.getParentFile().mkdirs();
            file.createNewFile();

            byte[] bytes = baos.toByteArray();
            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(bytes);
            }

            jis.close();
            baos.close();

            writeJarEntry("META-INF/", null);
            writeJarEntry("META-INF/MANIFEST.MF", bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void savePackage(JarItem jarItem) throws IOException {
        writeJarEntry(jarItem.getOriginalLocation(), null);
    }

    private void writeJarEntry(String name, byte[] bytes) throws IOException {
        JarEntry je = new JarEntry(name);
        jos.putNextEntry(je);
        if(bytes != null)
            jos.write(bytes);
    }
}
