package wat.jfk.jar.content;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

public class JarItem {
    private String name;
    private String fullName;
    private ItemType type;
    private String jarFile;

    private boolean addedThisSession;

    JarItem(String name, String fullName, ItemType type, String jarFile) {
        this.name = name;
        this.fullName = fullName;
        this.type = type;
        this.jarFile = jarFile;
    }

    public String getName() {
        return name;
    }

    public String getClassLocation() {
        return fullName.replace(".class", "").replace("/", ".");
    }

    public String getOriginalLocation() {
        return fullName;
    }

    public String getJarFile() {
        return jarFile;
    }

    public ItemType getType() {
        return type;
    }

    void setAddedThisSession(boolean addedThisSession) {
        this.addedThisSession = addedThisSession;
    }

    boolean isAddedThisSession() {
        return addedThisSession;
    }

    @Override
    public String toString() {
        return name + (addedThisSession ? "*" : "");
    }

    public void printAll() {
        System.out.println("jar item {");
        System.out.println("    name: " + name);
        System.out.println("    fullName: " + fullName);
        System.out.println("    type: " + type);
        System.out.println("    jarFile: " + jarFile);
        System.out.println("}");
    }

    public CtClass getClassObject() throws NotFoundException {
        return ClassPool.getDefault().get(getClassLocation());
    }
}
