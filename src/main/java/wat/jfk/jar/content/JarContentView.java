package wat.jfk.jar.content;

import wat.jfk.jar.mvc.View;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class JarContentView extends View<JarContentModel> {
    private JPanel panel;

    private JTree filesTree;
    private JButton addPackage;
    private JButton addClass;
    private JButton addInterface;
    private JButton deleteNode;

    public JarContentView(JarContentModel model) {
        super(model);
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        filesTree = new JTree();
        filesTree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode()));
        JPanel treePanel = new JPanel();
        treePanel.add(filesTree);
        JScrollPane scrollPane = new JScrollPane(treePanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);

        JPanel buttonsPanel = createButtonsPanel();
        panel.add(buttonsPanel);
        panel.add(scrollPane);
    }

    private JPanel createButtonsPanel() {
        addPackage = new JButton("Add package");
        addClass = new JButton("Add class");
        addInterface = new JButton("Add interface");
        deleteNode = new JButton("Delete");
        setButtonsEnabled(false);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5,5,5,5);
        gbc.gridy = 0;

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridBagLayout());
        gbc.gridx = 0;
        buttonsPanel.add(addPackage, gbc);
        gbc.gridx = 1;
        buttonsPanel.add(addClass, gbc);
        gbc.gridx = 2;
        buttonsPanel.add(addInterface, gbc);
        gbc.gridx = 3;
        buttonsPanel.add(deleteNode, gbc);

        buttonsPanel.setMaximumSize(new Dimension(500, 100));
        return buttonsPanel;
    }

    void setButtonsEnabled(boolean enabled) {
        addPackage.setEnabled(enabled);
        addClass.setEnabled(enabled);
        addInterface.setEnabled(enabled);

        if(!enabled)
            deleteNode.setEnabled(false);
    }

    private void setDeleteButtonEnabled(boolean enabled) {
        deleteNode.setEnabled(enabled && model.getCurrentlySelectedItem().isAddedThisSession());
    }

    @Override
    public void refresh() {
        filesTree.setModel(new DefaultTreeModel(model.getTreeRoot(), true));
        expandAllNodes(filesTree, 0, filesTree.getRowCount());
    }

    void insertNode(DefaultMutableTreeNode node, DefaultMutableTreeNode currentlySelected) {
        if(node != null) {
            DefaultTreeModel model = (DefaultTreeModel) filesTree.getModel();
            model.insertNodeInto(node, currentlySelected, 0);
        }
    }

    void deleteNode(DefaultMutableTreeNode node) {
        if(node != null) {
            DefaultTreeModel model = (DefaultTreeModel) filesTree.getModel();
            model.removeNodeFromParent(node);
        }
    }

    @Override
    public Component getContent() {
        return panel;
    }

    private void expandAllNodes(JTree tree, int startingIndex, int rowCount){
        for(int i=startingIndex;i<rowCount;++i) {
            tree.expandRow(i);
        }

        if(tree.getRowCount()!=rowCount) {
            expandAllNodes(tree, rowCount, tree.getRowCount());
        }
    }

    interface TreeItemSelector {
        void itemSelected(DefaultMutableTreeNode item);
    }

    void onTreeItemSelected(TreeItemSelector selector) {
        filesTree.addTreeSelectionListener(e -> {
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)filesTree.getLastSelectedPathComponent();

            if(selectedNode != null) {
                selector.itemSelected(selectedNode);

                JarItem item = (JarItem) selectedNode.getUserObject();
                setButtonsEnabled(item.getType() == ItemType.PACKAGE || item.getType() == ItemType.JAR_ROOT);
                setDeleteButtonEnabled(item.getType() == ItemType.CLASS || item.getType() == ItemType.INTERFACE || item.getType() == ItemType.PACKAGE);
                item.printAll();
            }
        });
    }

    void addPackageButtonListener(ActionListener onClicked) {
        addPackage.addActionListener(onClicked);
    }

    void addClassButtonListener(ActionListener onClicked) {
        addClass.addActionListener(onClicked);
    }

    void addInterfaceButtonListener(ActionListener onClicked) {
        addInterface.addActionListener(onClicked);
    }

    void addDeleteButtonListener(ActionListener onClicked) {
        deleteNode.addActionListener(onClicked);
    }


}
