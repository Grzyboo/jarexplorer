package wat.jfk.jar.content;

public enum ItemType {
    UNKNOWN, JAR_ROOT, CLASS, INTERFACE, PACKAGE, MANIFEST, TEXT, IMAGE, RESOURCE
}
