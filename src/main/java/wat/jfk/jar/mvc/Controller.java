package wat.jfk.jar.mvc;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;

public abstract class Controller<VIEW extends View<?>, MODEL> {
    protected VIEW view;
    protected MODEL model;

    public Controller() {
        try {
            createModelAndView();
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked cast")
    private void createModelAndView() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> viewClass = (Class<?>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        Class<?> modelClass = (Class<?>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];

        Constructor<?> modelConstructor = modelClass.getDeclaredConstructor();
        modelConstructor.setAccessible(true);
        model = (MODEL)modelConstructor.newInstance();
        view = (VIEW)viewClass.getDeclaredConstructor(modelClass).newInstance(model);
    }

    public VIEW getView() {
        return view;
    }
}
