package wat.jfk.jar.mvc;

public interface Refreshable {
    void refresh();
}
