package wat.jfk.jar.mvc;

import java.awt.*;

public abstract class View<MODEL> implements Refreshable {

    protected MODEL model;

    public View(MODEL model) {
        this.model = model;
    }

    public abstract Component getContent();
}
