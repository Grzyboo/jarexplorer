package wat.jfk;

import wat.jfk.jar.classes.ClassController;
import wat.jfk.jar.classes.body.BodyController;
import wat.jfk.jar.content.JarContentController;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private JMenuItem loadJar;
    private JMenuItem saveJar;

    MainFrame() {
        super("Jar Explorer");

        BodyController bodyController = new BodyController();
        ClassController classController = new ClassController(bodyController);
        JarContentController jarContentController = new JarContentController(bodyController, classController);
        bodyController.setClassController(classController);

        JPanel mainPanel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weighty = 1;
        gbc.insets = new Insets(20, 20, 20, 20);

        gbc.gridx = 0;
        gbc.weightx = 0;
        gbc.fill = GridBagConstraints.VERTICAL;
        mainPanel.add(jarContentController.getView().getContent(), gbc);
        gbc.gridx = 1;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(classController.getView().getContent(), gbc);
        gbc.gridx = 2;
        gbc.weightx = 0;
        gbc.fill = GridBagConstraints.VERTICAL;
        mainPanel.add(bodyController.getView().getContent(), gbc);

        add(mainPanel);

        createMenu();

        jarContentController.setListenersForMenuItems(loadJar, saveJar);

        setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    private void createMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = new JMenu("Jar");
        loadJar = new JMenuItem("Load jar...");
        saveJar = new JMenuItem("Save jar...");

        menu.add(loadJar);
        menu.add(saveJar);
        menuBar.add(menu);

        setJMenuBar(menuBar);
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(400, 300);
    }
}
