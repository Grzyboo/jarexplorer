package test.wat.jfk.jar;

import javassist.*;
import org.junit.jupiter.api.Test;

public class MethodBodyReplacementTest {

    @Test
    public void test2() throws Exception {
        ClassPool pool = ClassPool.getDefault();

        CtClass cc = pool.get("test.wat.jfk.jar.Hello");
        cc.defrost();
        CtMethod m = cc.getDeclaredMethod("say");

//        m.setBody("{ System.out.println(\"Replaced!\"); }");
//        m.insertBefore("{ value = 0; }");

        cc.writeFile(".");
        cc.toClass();

        Hello h2 = new Hello();
        h2.say(5);
    }
}
